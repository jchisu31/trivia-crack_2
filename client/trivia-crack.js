Accounts.ui.config({
    passwordsSignupFields: "USERNAME_ONLY"
});

var APPLICATION_ID,
    SECRET_KEY,
    VERSION = 'v1';

Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);